function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const coin = {
    state: 0,

    flip: function () {
        // Use "this.state" para acessar a propriedade "state".
        // Configure de forma randômica a propriedade “state” do
        // seu objeto moeda. "STATE" deve receber somente os valores 0 ou 1.
        this.state = getRandomIntInclusive(0, 1);
        return this.state;
    },

    toString: function () {
        // Se o valor de "state" for 0, retorne "Heads"
        // Se o valor de "state" for 1, retorne "Tails"
        if (this.state === 0) {
            return "Heads"; 
        }

        return "Tails";
    },

    toHTML: function () {
        const image = document.createElement("img");
        // Colocar uma imagem correspondente a esse valor.
        // image.src = "./CAMINHO/IMAGEM.JPEG"
        // image.alt = "Heads/Tails"
        if (this.state === 0) {
            image.src = "https://sorteador.com.br/_imgs/caracoroa/cara.jpg";
            image.height = 64;
            image.width = 64;
            image.alt = "Heads"
        } else {
            image.src = "https://sorteador.com.br/_imgs/caracoroa/coroa.jpg";
            image.height = 64;
            image.width = 64;
            image.alt = "Tails"
        }
        return image;
    },
};

function display20Flips() {
    const results = [];
    corpo = document.getElementById("corpo");
    // Use um loop para arremessar a moeda 20 vezes.
    // Depois que o seu loop terminar, exiba o resultado na página no formato de TEXTO.
    // Além de exibir os resultados na página, não esqueça
    // de retornar o valor de "results".
    // Caso esqueça de retornar "results", sua função não
    // irá passar nos testes.
    for (let i = 1; i <= 20; i++) {
        moeda = coin;
        moeda.flip();
        display = document.createElement("p");
        display.innerText = moeda.toString();
        corpo.appendChild(display);
        results.push(moeda.state);
    }

    return results;
}

function display20Images() {
    const results = [];
    corpo = document.getElementById("corpo");
    // Use um loop para arremessar a moeda 20 vezes.
    // Depois que o seu loop terminar, exiba o resultado na página no formato de IMAGEM.
    // Além de exibir os resultados na página, não esqueça
    // de retornar o valor de "results".
    // Caso esqueça de retornar "results", sua função não
    // irá passar nos testes.
    for (let i = 1; i <= 20; i++) {
        moeda = coin;
        moeda.flip();
        display = moeda.toHTML();
        corpo.appendChild(display);
        results.push(moeda.state);
    }

    return results;
}

console.log(display20Flips());
console.log(display20Images());
